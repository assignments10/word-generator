const { workerData, parentPort } = require('worker_threads');
const { getRandomWordSync, getRandomWord } = require('word-maker');

// bubble sort
function generateWords(i) {
    var word = getRandomWordSync({slow:true})
    return (`${i}: ${word}`)
}

parentPort.postMessage(
    generateWords(workerData.value)
);
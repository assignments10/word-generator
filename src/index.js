const { getRandomWordSync, getRandomWord } = require('word-maker');
const {Worker,isMainThread} = require('worker_threads');
const logger = require('./logger');
const path = require('path');

() => {

}

console.log('It works!');


const printBuzzFizz = (i,word) =>{
    if(i % 15 == 0) logger.info(`${i}: FizzBuzz`);
    else if (i % 3 == 0) logger.info(`${i}: Fizz`);
    else if (i % 5 == 0) logger.info(`${i}: Buzz`);
    else logger.info(`${i}: ${word}`);
}


//TODO: Task 1
logger.info('Initiating task 1')
for(var i = 1; i <= 100; i++){
    logger.info(`${i}: ${getRandomWordSync()}`)
}

//TODO: Task 2
logger.info('Initiating task 2')
for(var i = 1; i <= 100; i++){
    printBuzzFizz(i, getRandomWordSync())
}

//TODO: Task 3 part 1
logger.info('Initiating task 3 part 1')
const runner = async () => {
    for(var i = 1; i <= 100; i++){
        var word = await getRandomWord();
        logger.info(`${i}: ${word}`)
    }
}

runner()

// TODO: Task 3 part 2
logger.info('Initiating task 3 part 2')
const runnerBuzz = async () => {
    for(var i = 1; i <= 100; i++){
        var word = await getRandomWord();
        printBuzzFizz(i,word)
    }
}

runnerBuzz()

//TODO: Task 4 part 1
logger.info('Initiating task 4 part 1')
const runnerWithErrors = async () => {
    for(var i = 1; i <= 100; i++){
        try {
            var word = await getRandomWord({withErrors:true});
            logger.info(`${i}: ${word}`)
        } catch (error) {
            logger.error(`It shouldn't break anything!`)
        }
    }
}

runnerWithErrors()

//TODO: Task 4 part 2
logger.info('Initiating task 4 part 2')
const runnerBuzzWithErrors = async () => {
    for(var i = 1; i <= 100; i++){
        try {
            var word = await getRandomWord({withErrors:true});
            printBuzzFizz(i,word)
        } catch (error) {
            logger.error(`It shouldn't break anything!`)
        }
    }
}

runnerBuzzWithErrors()

//TODO: 5 add logger
logger.info('Initiating task 5')

//TODO: Bonus 
logger.info('Initiating bonus tasks')

//FIXME: order of worker thread completion. 
const runnerSlow = async () => {
    for(var i = 1; i <= 100; i++){
        const worker = new Worker(path.join(__dirname, 'worker.js'),{ workerData: { value: i } })
        worker.on('message', (result) => {
            console.log(result);
        });
    }
}

runnerSlow();

// Recursion implementation 
logger.info('Initiating recursive implementation')
const printRecursive = (size) => {
    if(size == 101) return
    else{
        console.log(`${size}: ${getRandomWordSync()}`)
        printRecursive(size + 1)
    }
}

printRecursive(1)

const printRecursiveAscending= (size) => {
    if(size == 0) return
    else{
        console.log(`${size}: ${getRandomWordSync()}`)
        printRecursiveAscending(size - 1)
    }
}

printRecursiveAscending(100)


const { createLogger, format, transports } = require('winston');
const path = require('path');

const FILE_NAME = 'server.log';
const LOG_FILE_PATH = path.join(__dirname, '..', FILE_NAME);
const {
  combine, timestamp, label, printf,
} = format;

const customFormat = printf(({
  level, message, label, timestamp,
}) => `${timestamp} [${label}] ${level}: ${message}`);

const options = {
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
        },
    file: {
        filename: LOG_FILE_PATH,
    }
};

const logger = createLogger({
  format: combine(
    label({ label: 'Task runner' }),
    timestamp(),
    customFormat,
  ),
  transports: [
    new transports.Console(options.console),
    new transports.File(options.file),
  ],
});

module.exports = logger;